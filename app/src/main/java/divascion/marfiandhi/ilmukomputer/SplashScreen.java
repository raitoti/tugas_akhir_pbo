package divascion.marfiandhi.ilmukomputer;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;

public class SplashScreen extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash_screen);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                move();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
    private void move() {
        Intent mainIntent = new Intent(this, LoginActivity.class);
        startActivity(mainIntent);
        finish();
    }
}
