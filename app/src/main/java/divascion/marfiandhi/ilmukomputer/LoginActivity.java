package divascion.marfiandhi.ilmukomputer;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class LoginActivity extends AppCompatActivity {

    // UI references.
    private static final String TAG = "";
    private View mSignIn;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN;
    private String personName, personGivenName, personFamilyName, personEmail, personId;
    private Uri personPhoto;
    private boolean login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mSignIn = findViewById(R.id.GoogleSignIn);
        mSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.GoogleSignIn:
                        signIn();
                        break;
                }
            }
        });
        if(!login) {
            signOut();
        }
    }

    protected boolean out(boolean x) {
        login = x;
        return login;
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void getData(GoogleSignInAccount account) {
        personName = account.getDisplayName();
        personGivenName = account.getGivenName();
        personFamilyName = account.getFamilyName();
        personEmail = account.getEmail();
        personId = account.getId();
        personPhoto = account.getPhotoUrl();
    }

    private void updateUI( GoogleSignInAccount account) {
        if(account != null) {
            getData(account);
            moveActivity();
        } else {

        }
    }

    private void moveActivity() {
        Intent intent = new Intent(this, MainScreenActivity.class);
        intent.putExtra("name", personName);
        intent.putExtra("givenName", personGivenName);
        intent.putExtra("familyName", personFamilyName);
        intent.putExtra("email", personEmail);
        intent.putExtra("id", personId);
        intent.putExtra("photo", personPhoto);
        startActivity(intent);
    }
}

