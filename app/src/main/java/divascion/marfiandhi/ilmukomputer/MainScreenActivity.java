package divascion.marfiandhi.ilmukomputer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import divascion.marfiandhi.ilmukomputer.model.UploadInfo;

public class MainScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String personName, personGivenName, personFamilyName, personEmail, personId, personPhoto, task, fileName, labAssist;
    private StorageReference mStorageRef;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDataRef;
    private UploadTask uploadTask;
    private String TAG;
    private int FILE_SELECT_CODE;
    private Uri filePath;
    private WebView myWebView;
    private Toolbar mActionBarToolbar;
    private Spinner dropdown;
    private EditText mfileName;

    @SuppressLint({"SetJavaScriptEnabled", "CutPasteId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        mDatabase = FirebaseDatabase.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mDataRef = mDatabase.getReference();

        mDataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        dropdown = findViewById(R.id.task);
        String[] items = new String[]{"Tugas 1", "Tugas 2", "Tugas 3", "Tugas 4", "Tugas 5", "Tugas 6", "Tugas 7", "Tugas 8"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
        task = dropdown.getSelectedItem().toString();

        mfileName = findViewById(R.id.file_name);
        Intent intent = getIntent();
        personName = intent.getStringExtra("name");
        personGivenName = intent.getStringExtra("givenName");
        personFamilyName = intent.getStringExtra("familyName");
        personEmail = intent.getStringExtra("email");
        personId = intent.getStringExtra("id");
        personPhoto = intent.getStringExtra("photo");

        myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl("http://pikf.blogspot.co.id/search/label/Lab");
        WebSettings webSettingshome = myWebView.getSettings();
        webSettingshome.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());

        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        mActionBarToolbar.setTitle("Main Menu");

        Button choose = (Button) findViewById(R.id.choose);
        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileChooser();
            }
        });

        Button upload = (Button) findViewById(R.id.upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    upload();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView name = headerView.findViewById(R.id.viewName);
        name.setText(personName);
        TextView email = headerView.findViewById(R.id.viewEmail);
        email.setText(personEmail);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private Boolean exit = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(myWebView.canGoBack()) {
            myWebView.goBack();
        } else {
            finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_upload) {
            View view = (View) findViewById(R.id.view_upload);
            view.setVisibility(View.VISIBLE);
            View view2 = (View) findViewById(R.id.scrollViewWeb);
            view2.setVisibility(View.GONE);
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            mActionBarToolbar.setTitle("Upload");
        } else if(id == R.id.nav_menu) {
            View view = (View) findViewById(R.id.view_upload);
            view.setVisibility(View.GONE);
            View view2 = (View) findViewById(R.id.scrollViewWeb);
            view2.setVisibility(View.VISIBLE);
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            mActionBarToolbar.setTitle("Main Menu");
        } else if (id == R.id.nav_info) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_log_out) {
            Intent intent = new Intent(this, LoginActivity.class);
            finish();
            LoginActivity log = new LoginActivity();
            boolean bye = false;
            log.out(bye);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void fileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FILE_SELECT_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            String src = data.getData().getLastPathSegment();
            Button upload = (Button) findViewById(R.id.upload);
            upload.setClickable(true);
            AutoCompleteTextView source = (AutoCompleteTextView) findViewById(R.id.file_source);
            source.setHint(src);
            upload.setClickable(true);
        }
    }

    private void upload() {
        if(filePath != null && mfileName != null) {
            try {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading File");
                progressDialog.show();

                fileName = mfileName.getText().toString();
                String id = personEmail;
                assistName();
                StorageMetadata metadata = new StorageMetadata.Builder()
                        .setContentType("Files")
                        .build();
                uploadTask = mStorageRef.child("Lab_PIK/"+labAssist+"/"+id+"/"+task+"/"+fileName+"."+getFileExtension(filePath)).putFile(filePath, metadata);
                uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.dismiss();
                        String name = taskSnapshot.getMetadata().getName();
                        String url = taskSnapshot.getDownloadUrl().toString();
                        writeNewImageInfoToDB(name, url);
                    }
                });
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_SHORT);
        }
    }
    private void writeNewImageInfoToDB(String name, String url) {
        UploadInfo info = new UploadInfo(name, url);

        String key = mDataRef.push().getKey();
        mDataRef.child(key).setValue(info);
    }
    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();

        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void assistName() {
        String id = personEmail;
        if(id.equals("marfiandhiputra@gmail.com") || id.equals("achmad.syarif.hidayatullah@gmail.com") || id.equals("akbar.atori85@gmail.com") || id.equals("bagasprasetyo36@gmail.com") || id.equals("deovrjr@gmail.com") || id.equals("dzulrahmat17@gmail.com")) {
            labAssist = "Gibran";
        } else if(id.equals("faturrahmanavoiders@gmail.com") || id.equals("g.srimulyani@yahoo.co.id") || id.equals("kasim.baharuddin@gmail.com") || id.equals("kurnia3703@gmail.com") || id.equals("mreza447@gmail.com")) {
            labAssist = "Sule";
        } else if(id.equals("nurmayulina147@gmail.com") || id.equals("rizkasyahfitriii@gmail.com") || id.equals("sthestiana@gmail.com") || id.equals("tasniaakil2@gmail.com") || id.equals("mutawallysyarawy@gmail.com")) {
            labAssist = "Ilham";
        } else {
            labAssist = "Unknown";
        }
    }
}